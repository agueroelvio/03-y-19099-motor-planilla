
const express = require('express');
const hbs = require('hbs');
const app = express();
require("dotenv").config();




// importamos archivos de rutas
const router = require("./routes/public");
const { config } = require('dotenv');
// incluir la ruta dentro de express
app.use("/", router);
//creacion de aplicacion express
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

hbs.registerPartials(__dirname + "/views/partials");


const puerto = process.env.PORT || 3000;


app.listen(puerto, () => {
    console.log(`El servidor se está ejecutando en ${puerto}`);

});
 
