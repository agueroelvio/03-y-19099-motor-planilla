-- Tabla Integrante
CREATE TABLE IF NOT EXISTS Integrante (
  matricula VARCHAR(30) PRIMARY KEY,
  nombre VARCHAR(30) NOT NULL,
  apellido VARCHAR(30) NOT NULL,
  rol VARCHAR(50) NOT NULL,
  codigo VARCHAR(255) NOT NULL,
  url VARCHAR(255) NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE
);

-- Tabla TipoMedia
CREATE TABLE IF NOT EXISTS TipoMedia (
  tipo VARCHAR(250) PRIMARY KEY,
  descripcion TEXT NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE
);

-- Tabla Media
CREATE TABLE IF NOT EXISTS Media (
  id INTEGER PRIMARY KEY,
  matricula VARCHAR(30) NOT NULL,
  titulo VARCHAR(30) NOT NULL,
  parrafo TEXT NOT NULL,
  imagen TEXT,
  video TEXT,
  html TEXT,
  tipo_media VARCHAR(50) NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (matricula) REFERENCES Integrante(matricula),
  FOREIGN KEY (tipo_media) REFERENCES TipoMedia(tipo)
);

-- Insertar datos para Integrantes
INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado)
VALUES 
  ('Y25387', 'Junior', 'Cabral', 'Desarrollador', '02', 'Y25387/index.html', FALSE),
  ('Y25495', 'Sebastian', 'Duarte', 'Desarrollador', '03', 'Y25495/index.html', FALSE),
  ('Y19099', 'Elvio', 'Aguero', 'Desarrollador', '04', 'Y19099/index.html', FALSE),
  ('UG0085', 'Luis', 'Delgado', 'Desarrollador', '05', 'UG0085/index.html', FALSE);

-- Insertar datos para TipoMedia
INSERT INTO TipoMedia (tipo, descripcion, esta_borrado)
VALUES
  ('Youtube', 'Videos en linea', FALSE),
  ('Twitter/X', 'Mensajes cortos y multimedia', FALSE),
  ('Dibujo', 'Ilustraciones y gráficos', FALSE);

-- Insertar datos para Media
INSERT INTO Media (matricula, titulo, parrafo, tipo_media, esta_borrado)
VALUES
  ('Y25387', 'Mi Video Favorito en YouTube.', 'Uno de los vídeos más interesantes para mí trata sobre la idea preconcebida de que estamos hechos de partículas, sino de algo más, tal vez campos que vibran en el espacio y en base a esas fluctuaciones existimos.', 'Youtube', FALSE),
  ('Y25387', 'Imagen Favorita', 'Una de las cosas que más me gusta hacer es observar el atardecer. Es un momento de paz y tranquilidad que me ayuda a relajarme y pensar en las cosas que me importan, nunca dejando de cuestionarme los hechos físicos que llevan a que se puedan observar, como en esta imagen', 'Imagen', FALSE),
  ('Y25387', 'Dibujo Propio', 'Algo que me apasiona bastante, además de la física de partículas y los atardeceres, es poder mirar más allá de donde estamos, imaginar los mundos posibles en la vastedad del cosmos y lo impresionante que son, como este dibujo de un planeta con un sistema de anillos parecido al de Júpiter.', 'Dibujo', FALSE),
  ('Y25495', 'Mi Video Favorito en YouTube.', 'Mi video favorito es el trailer de la última temporada de mi serie favorita, considerada una obra maestra narrativa y visual. Para mí, simboliza el fin de una etapa en mi vida, ya que seguí esta serie desde hace 8 años.', 'Youtube', FALSE),
  ('Y25495', 'Imagen Favorita', 'Mi imagen favorita es el protagonista del videojuego Dark Souls, y la imagen representa para mí su desesperanza y su impotencia, pero al mismo tiempo el objetivo que lo mantiene en su travesía. Me gusta por todo lo que representa para mí y todo lo significó para mí el juego y su historia bien elaborada.', 'Imagen', FALSE),
  ('Y25495', 'Dibujo Propio', 'Mi dibujo siento que representa la perseverancia, ya que esta fogata es aquella en la que los aventureros descansan y se arman de valor para seguir con su viaje. También está sacada del videojuego Dark Souls.', 'Dibujo', FALSE),
  ('Y19099', 'Mi Video Favorito en YouTube.', 'Uno de los vídeos que más me llama la atención es acerca de un libro que se llama La Vida Secreta De La Mente.', 'Youtube', FALSE),
  ('Y19099', 'Imagen Favorita', 'Mi imagen favorita es el protagonista del videojuego Dark Souls, y la imagen representa para mí su desesperanza y su impotencia, pero al mismo tiempo el objetivo que lo mantiene en su travesía. Me gusta por todo lo que representa para mí y todo lo significó para mí el juego y su historia bien elaborada.', 'Imagen', FALSE),
  ('Y19099', 'Dibujo Propio', 'Mi dibujo siento que representa la perseverancia, ya que esta fogata es aquella en la cual los aventureros descansan y se arman de valor para seguir con su viaje. También está sacada del videojuego Dark Souls.', 'Dibujo', FALSE);