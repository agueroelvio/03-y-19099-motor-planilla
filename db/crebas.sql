-- Tabla Integrante
CREATE TABLE IF NOT EXISTS Integrante (
  matricula VARCHAR(30) PRIMARY KEY,
  nombre VARCHAR(30) NOT NULL,
  apellido VARCHAR(30) NOT NULL,
  rol VARCHAR(50) NOT NULL,
  codigo VARCHAR(255) NOT NULL,
  url VARCHAR(255) NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE
);

-- Tabla TipoMedia
CREATE TABLE IF NOT EXISTS TipoMedia (
  tipo VARCHAR(250) PRIMARY KEY,
  descripcion TEXT NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE
);

-- Tabla Media
CREATE TABLE IF NOT EXISTS Media (
  id INTEGER PRIMARY KEY,
  matricula VARCHAR(30) NOT NULL,
  titulo VARCHAR(30) NOT NULL,
  parrafo TEXT NOT NULL,
  imagen TEXT,
  video TEXT,
  html TEXT,
  tipo_media VARCHAR(50) NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (matricula) REFERENCES Integrante(matricula),
  FOREIGN KEY (tipo_media) REFERENCES TipoMedia(tipo)
);

