// archivo que contiene todas las rutas de mi proyecto
const db = require('../db/data');
const express = require("express");
const router = express.Router();
const dbSqlite = require("./db/conexion");
 
// las rutas de mi proyecto
//correccion de rutas
router.get("/", (request, response) => {
    const matriculaDeHome = "Home";
    const Home = [];
    for (indice in db.media) {
        if (matriculaDeHome === db.media[indice].matricula)
            Home.push(db.media[indice]);
    }
    console.log("DatosDeHome", Home);
    response.render("index", {
        data: Home,
        base_url: "http://localhost:3000",
        integrantes: db.integrantes
    });
});
router.get("/integrantes/:matricula", (request, response) => {
    const matricula = request.params.matricula;

    let integrante = {};
    for (let indice in db.integrantes) {
        if (matricula === db.integrantes[indice].matricula) {
            integrante = db.integrantes[indice];
        }
    }
    const datos = [];
    for (let indice in db.media) {
        if (matricula === db.media[indice].matricula) {
            datos.push(db.media[indice]);
        }
    }

    if (Object.keys(integrante).length === 0) {
        response.status(404).render("error");
    } else {
        response.render("integrante", {
            data: datos,
            base_url: "http://localhost:3000",
            integrantes: db.integrantes,
        });
    }
});
router.get("/paginas/word_cloud.html", (request, response) => {
    response.render("word_cloud",{
        integrantes: db.integrantes,
    });
});
router.get("/paginas/curso.html", (request, response) => {
    response.render("curso",{
        integrantes: db.integrantes,
    });
    
});

// eportamos el router
module.exports = router;
